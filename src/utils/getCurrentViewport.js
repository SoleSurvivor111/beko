/* eslint-disable no-unused-vars */

import {
  BREAKPOINT_FULL_HD,
  BREAKPOINT_MEDIUM,
  BREAKPOINT_DESKTOP,
  BREAKPOINT_TABLET,
  BREAKPOINT_TABLET_PORTRAIT,
  BREAKPOINT_MOBILE,
  BREAKPOINT_MOBILE_SMALL,
  FULL_HD_HEIGHT,
  FULL_HD,
  TABLET_PORTRAIT,
  TABLET,
  DESKTOP,
  MEDIUM,
  MOBILE,
  MOBILE_SMALL,
} from 'constants/app';

const getWindowDimensions = () => {
  // if (!__CLIENT__) {
  //   return {
  //     windowWidth: BREAKPOINT_FULL_HD,
  //     windowHeight: FULL_HD_HEIGHT,
  //   };
  // }

  return {
    windowWidth: document.documentElement.clientWidth,
    windowHeight: document.documentElement.clientHeight,
  };
};

const getViewportType = (windowWidth, windowHeight) => {
  if (windowWidth <= BREAKPOINT_MOBILE_SMALL) return MOBILE_SMALL;
  if (windowWidth <= BREAKPOINT_MOBILE) return MOBILE;
  if (windowWidth <= BREAKPOINT_TABLET_PORTRAIT) return TABLET_PORTRAIT;
  if (windowWidth <= BREAKPOINT_TABLET) return TABLET;
  if (windowWidth <= BREAKPOINT_DESKTOP) return DESKTOP;
  if (windowWidth <= BREAKPOINT_MEDIUM) return MEDIUM;

  return FULL_HD;
};

const isHighDensity = () => (
  (window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) ||
  (window.devicePixelRatio && window.devicePixelRatio > 1.3)
);

export default () => {
  // if (!__CLIENT__) {
  //   return {
  //     isHighDensity: false,
  //     windowWidth: BREAKPOINT_FULL_HD,
  //     windowHeight: FULL_HD_HEIGHT,
  //     viewportType: FULL_HD,
  //   };
  // }

  const {
    windowWidth,
    windowHeight,
  } = getWindowDimensions();

  return {
    isHighDensity: isHighDensity(),
    windowWidth,
    windowHeight,
    viewportType: getViewportType(windowWidth, windowHeight),
    isMobile: windowWidth <= 640 ? true : false,
  };
};
