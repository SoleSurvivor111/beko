import React from 'react';
import classnames from 'classnames';

import backgroundImg from 'images/pictures/kitchen-mobile.png';

import SearchBeko from './SearchBeko';
import { ReactComponent as Logo } from 'images/icons/beko-logo-icon-big.svg';

import s from './MobileMain.module.scss';

export default class MobileMain extends React.Component {
  state = {
    isSearchActive: false,
    isBottomBarActive: true,
  }

  render() {
    const { isBottomBarHide } = this.props;
    return (
      <main className={s.root}>
        <img
          src={backgroundImg}
          alt=""
          className={s['background-img']}
        />
        <Logo
          width="66"
          height="38"
          viewBox="0 0 66 38"
          className={s.logo}
        /> 
        <div
          className={classnames(
            s['bottom-bar'],
            { __hide: isBottomBarHide },
          )}>
          <SearchBeko />
        </div>
      </main>
    );
  }
}
