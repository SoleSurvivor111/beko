import React, { Component } from 'react';
import classnames from 'classnames';

import { ReactComponent as SearchIcon } from 'images/icons/magnifier-icon.svg';

import s from './SearchBeko.module.scss';

export default class SearchBeko extends Component {
  state = {
    isSearchActive: false,
    isSearchFullScreen: false,
  }

  handleActivateSearch = () => {
    const { isSearchActive } = this.state;

    this.setState({ isSearchActive: !isSearchActive});
  }

  handleActivateFullScreenSearch = () => this.setState({ isSearchFullScreen: true});
  handleDisableFullScreenSearch = () => this.setState({ isSearchFullScreen: false})
  

  render() {
    const {
      isSearchActive,
      isSearchFullScreen,
    } = this.state;
    const { style } = this.props;
    return (
      <div
        className={classnames(
          style,
          s.root,
          { __active: isSearchActive },
          { '__search-full-screen': isSearchFullScreen },
          )}
      >
        <input
          className={s.input}
          type="text"
          placeholder="Search Beko"
          onFocus={this.handleActivateFullScreenSearch}
        />
        <div
          className={s['search-icon']}
          onClick={!isSearchFullScreen && this.handleActivateSearch}
        >
          <SearchIcon />
        </div>
          <div className={s.dropdown}>
            <h1
              className={s.title}
            >
              Need some inspiration?
            </h1>
            <ul className={s['search-list']}>
              <ol
                className={s.item}
              >
                American Style Fridgefreezers
              </ol>
              <ol
                className={s.item}
              >
                How To Set Up A Washing Machine
              </ol>
              <ol
                className={s.item}
              >
                Eat Like A Pro Recipies
              </ol>
            </ul>
            <button
              type="button"
              className={s["close-btn"]}
              onClick={this.handleDisableFullScreenSearch}
            />
          </div>
      </div>
    );
  }
}
