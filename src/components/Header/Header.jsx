import React from 'react';

import logoImg from 'images/pictures/beko-logo.png';
import { ReactComponent as CompareIcon } from 'images/icons/compare-icon.svg';
import { ReactComponent as WhishListIcon } from 'images/icons/heart-icon.svg';
import { ReactComponent as LanguageIcon } from 'images/icons/language-icon.svg';
import { ReactComponent as TruckIcon } from 'images/icons/truck-icon.svg';
import { ReactComponent as UserSettingsIcon } from 'images/icons/user-settings-icon.svg';
import { ReactComponent as MapLocationIcon } from 'images/icons/map-location-icon.svg';

import Search from './Search';

import s from './Header.module.scss';

const additionallyList = [
  {
    Icon: WhishListIcon,
    value: 'Wishlist',
    quantity: '3',
  },
  {
    Icon: CompareIcon,
    value: 'Compare',
    quantity: '2',
  },
];
const navList = ['Products', 'Support', 'About Beko', 'Blog'];
const settingsList = [
  { Icon: LanguageIcon },
  { Icon: UserSettingsIcon },
  { Icon: TruckIcon },
  { Icon: MapLocationIcon },
];

const Header = () => (
  <header className={s.root}>
    <div className={s.content}>
      <div className={s.additionally}>
        {additionallyList.map(({ value, quantity, Icon }) => (
          <div className={s.item} key={value}>
            <Icon />
            <span className={s.value}>{value}</span>
            <span className={s.quantity}>{quantity}</span>
          </div>
        ))}
      </div>
      <div className={s.menu}>
        <div className={s['nav-box']}>
          <ul className={s['nav-list']}>
            <img
              src={logoImg}
              alt="beko-logo"
              className={s.logo}
            />
            {navList.map((item) => (<li key={item}><a href="*" className={s.nav}>{item}</a></li>))}
          </ul>
          <ul className={s['settings-list']}>
            {settingsList.map(({ Icon }, idx) => (
              <li
                key={idx}
                className={s['settings-item']}
              >
                <Icon className={s.svg} />
              </li>
            ))}
          </ul>
        </div>
        <Search />
      </div>

    </div>
  </header>
);

export default Header;
