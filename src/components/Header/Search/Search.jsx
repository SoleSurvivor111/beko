import React from 'react';

import { ReactComponent as MagnifierIcon } from 'images/icons/magnifier-icon.svg';

import s from './Search.module.scss';

const Search = () => (
  <div className={s.root}>
    <input
      type="text"
      className={s.search}
      placeholder="Search Beko"
    />
    <MagnifierIcon
      className={s.icon}
    />
  </div>
);

export default Search;
