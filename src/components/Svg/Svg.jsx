import React from 'react';
import PropTypes from 'prop-types';


const Svg = ({
  width,
  height,
  className,
  Icon,
}) => (
  <Icon
    width={width}
    height={height}
    className={className}
    viewBox={`0 0 ${width} ${height}`}
  />
);

Svg.propTypes = {
  className: PropTypes.string.isRequired,
  Image: PropTypes.string.isRequired,
};

export default Svg;
