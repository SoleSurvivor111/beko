import React from 'react';

import { ReactComponent as LogoIcon } from 'images/icons/beko-logo-icon.svg';

import s from './MobileHeader.module.scss';

const MobileHeader = () => (
  <header className={s.root}>
    <LogoIcon
      className={s.logo}
      width={31}
      height={18}
  viewBox="0 0 31 18"
    />
    <div className={s.text}>
      / Products / Built in / Built in Landing
    </div>
  </header>
);

export default MobileHeader;
