import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _throttle from 'lodash/throttle';

import getCurrentViewport from 'utils/getCurrentViewport';

import * as actions  from '_redux/actions';

import Header from 'components/Header';
import MobileHeader from 'components/MobileHeader';
import Main from 'components/Main';
import MobileMain from 'components/MobileMain';
import Footer from 'components/Footer';

import { viewportSelector } from '_redux/selectors';

import './App.scss';

class App extends React.Component {
  state = {
    isBottomBarHide: false,
  }

  lastScrollTop = 0;

  componentDidMount() {
    this.handleWindowResize();
    window.addEventListener('resize', _throttle(this.handleWindowResize, 200));
    window.addEventListener('scroll', _throttle(this.handleScroll, 200));
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.handleWindowResize);
    window.removeEventListener('scroll', this.handleScroll);
  }
  
  handleScroll = () => {
    let st = window.pageYOffset || document.documentElement.scrollTop;

    if (st > this.lastScrollTop) {
      this.setState({ isBottomBarHide: true })
    } else {
      this.setState({ isBottomBarHide: false })
    }
    this.lastScrollTop = st;
  }

  handleWindowResize = () => {
    const currentViewport = getCurrentViewport();
    
    const {
      changeViewport,
      viewportInfo: { viewportType },
    } = this.props;

    const { viewportType: currentViewportType} = currentViewport;

    if (viewportType !== currentViewportType) {
      changeViewport(currentViewport);
    }
  };

  render() {
    const { isBottomBarHide } = this.state;
    const {
      viewportInfo: {
        isMobile,
      }
    } = this.props;
    return (
      <div
        className="App"
      >
        {isMobile ? <MobileHeader /> : <Header />} 
        {isMobile ? <MobileMain isBottomBarHide={isBottomBarHide} /> : <Main />}
        <Footer />
      </div>
    );
  }
}

export default connect(
  state => ({
    viewportInfo: viewportSelector(state),
  }),
  dispatch => ({
    ...bindActionCreators(actions, dispatch),
  }))(App);
