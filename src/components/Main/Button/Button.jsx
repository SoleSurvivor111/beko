import React from 'react';

import { ReactComponent as Image } from 'images/icons/arrow-icon.svg';

import s from './Button.module.scss';

const Button = () => (
  <div className={s.root}>
    <div className={s.value}>
      See All
    </div>
    <div className={s.icon}>
      <Image
        className={s.svg}
      />
    </div>
  </div>
);

export default Button;
