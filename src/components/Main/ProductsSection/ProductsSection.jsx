import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-animated-css';

import Button from '../Button';

import s from './ProductsSection.module.scss';

export default class ProductsSection extends PureComponent {
  static propTypes = {
    title: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
  }

  state = {
    isActiveAnimation: false,
  }

  handleActivateAnimation = () => this.setState({ isActiveAnimation: true })
  handleDisableAnimation = () => this.setState({ isActiveAnimation: false })

  render() {
    const { isActiveAnimation } = this.state;
    const {
      title,
      categories,
      img,
    } = this.props;
    return (
      <div
        key={title.value}
        className={s.section}
        onMouseEnter={this.handleActivateAnimation}
        onMouseLeave={this.handleDisableAnimation}
      >
        <img
          src={img}
          alt=""
          className={s['section-img']}
        />
        <Animated
          animationIn="fadeInUp"
          animationOut="none"
          isVisible={isActiveAnimation}
        >
          <h1 className={s.title}>{title.value}</h1>
        </Animated>
        <div className={s.categories}>
          {
            categories.map(({ value }, index) => (
              <Animated
                animationIn="fadeInUp"
                animationInDelay={index * 100}
                animationOut="none"
                isVisible={isActiveAnimation}
              >
                <p
                  key={value}
                  className={s.category}
                >
                  {value}
                </p>
              </Animated>

            ))
          }
        </div>
        <Animated
          animationIn="fadeInUp"
          animationOut="none"
          animationInDelay={600}
          isVisible={isActiveAnimation}
          className={s.btn}
        >
          <Button />
        </Animated>
        
      </div>
    );
  }
}
