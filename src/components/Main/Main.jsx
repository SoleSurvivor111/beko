import React from 'react';

import mainImage from 'images/pictures/beko-background-image.png';
import kitchenImg from 'images/pictures/cook.png';
import laundryImg from 'images/pictures/laundry.png';

import ProductsSection from './ProductsSection';

import s from './Main.module.scss';

const section = [
  {
    title: { value: 'Kitchen' },
    categories: [
      { value: 'Cooling' },
      { value: 'Cooking' },
      { value: 'Dishwashers' },
      { value: 'Appliances' },
    ],
    img: kitchenImg,
  },
  {
    title: { value: 'Kitchen' },
    categories: [
      { value: 'Cooling' },
      { value: 'Cooking' },
      { value: 'Dishwashers' },
      { value: 'Appliances' },
    ],
    img: laundryImg,
  },
  {
    title: { value: 'Kitchen' },
    categories: [
      { value: 'Cooling' },
      { value: 'Cooking' },
      { value: 'Dishwashers' },
      { value: 'Appliances' },
    ],
    img: laundryImg,
  },
  {
    title: { value: 'Kitchen' },
    categories: [
      { value: 'Cooling' },
      { value: 'Cooking' },
      { value: 'Dishwashers' },
      { value: 'Appliances' },
    ],
    img: kitchenImg,
  },
];

const Main = () => (
  <main className={s.main}>
    <img
      src={mainImage}
      alt=""
      className={s['main-img']}
    />
    {
      section.map(({
        title,
        categories,
        img,
      }) => (
        <ProductsSection
          key={title.value}
          title={title}
          categories={categories}
          img={img}
        />
      ))
    }
  </main>
);
export default Main;
