import React from 'react';

import logo from 'images/pictures/beko-logo-and-fcb-logo.png';
import { ReactComponent as FaceBookIcon } from 'images/icons/facebook-icon.svg';

import s from './SubscribeBlock.module.scss';

const address = [
  {
    value: 'Unitied Kinghtom',
  },
];

const socialNetwork = [
  {
    Icon: FaceBookIcon,
    id: 'facebook',
  },
  {
    Icon: FaceBookIcon,
    id: 'inst',
  },
  {
    Icon: FaceBookIcon,
    id: 'twitter',
  },
  {
    Icon: FaceBookIcon,
    id: 'vk',
  },
];

const subscribeBtn = { value: 'Subscribe To Our Newsletter' };

const SubscribeBlock = () => (
  <section className={s.root}>
    <div className={s['location-section']}>
      <img
        src={logo}
        alt=""
        className={s.logo}
      />
      <button
        className={s['change-location']}
        type="button"
      >
        {address[0].value}
      </button>
    </div>
    <div className={s['subscribe-section']}>
      <div className={s['social-network']}>
        {
        socialNetwork.map(({ Icon, id }) => (
          <div className={s.icon} key={id}>
            <Icon />
          </div>
        ))
        }
      </div>
      <button
        type="button"
        className={s['subscribe-btn']}
      >
        {subscribeBtn.value}
      </button>
    </div>
  </section>
);

export default SubscribeBlock;
