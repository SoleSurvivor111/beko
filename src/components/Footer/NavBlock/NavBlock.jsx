import React from 'react';

import Collaspsible from 'react-collapsible';

import s from './NavBlock.module.scss';

const navColumns = [
  {
    column: {
      title: { value: 'Kitchen' },
      content: [],
    },
    subColumn: [
      {
        subtitle: { value: 'Cooling' },
        content: [
          {
            value: 'Fridges',
          },
          {
            value: 'Freezers',
          },
          {
            value: 'Fridge Freezers',
          },
          {
            value: 'Integraged Fridges',
          },
          {
            value: 'Integrated Freezers',
          },
        ],
      },
      {
        subtitle: { value: 'Cooking' },
        content: [
          {
            value: 'Build-in Ovens',
          },
          {
            value: 'Buillt-in HOBs',
          },
          {
            value: 'Hoods',
          },
          {
            value: 'Built-in Microwaves',
          },
          {
            value: 'Warning Drawers',
          },
          {
            value: 'Freestanding Cookers',
          },
          {
            value: 'Microwaves',
          },
        ],
      },
      {
        subtitle: { value: 'Dishwashers' },
        content: [
          {
            value: 'Dishwashers',
          },
          {
            value: 'integrated Dishwashers',
          },
        ],
      },
      {
        subtitle: { value: 'Small Kitchen Appliances' },
        content: [
          {
            value: 'Coffee Machines',
          },
          {
            value: 'Juicers',
          },
          {
            value: 'Blenders',
          },
          {
            value: 'Choppers',
          },
          {
            value: 'Mixers',
          },
          {
            value: 'Toasters',
          },
          {
            value: 'Grill Toasters',
          },
          {
            value: 'Kettles',
          },
        ],
      },
    ],
  },
  {
    column: {
      title: { value: 'Support' },
      content: [
        {
          value: 'Help Center',
        },
        {
          value: 'Register & Warranty',
        },
        {
          value: 'Contact Support',
        },
        {
          value: 'Parts & Accessories',
        },
        {
          value: 'Safety',
        },
      ],
    },
    subColumns: [],
  },
  {
    column: {
      title: { value: 'Laundry' },
      content: [
        {
          value: 'Washing Machines',
        },
        {
          value: 'Tumble Dryers',
        },
        {
          value: 'Washer Dryers',
        },
        {
          value: 'Integrated Washing Machines',
        },
        {
          value: 'Integrated Washer Dryers',
        },
        {
          value: 'Irons',
        },
      ],
    },
    subColumns: [],
  },
  {
    column: {
      title: { value: 'Built-in' },
      content: [],
    },
    subColumn: [
      {
        subtitle: { value: 'Cooling' },
        content: [
          {
            value: 'Integraged Fridges',
          },
          {
            value: 'Integrated Freezers',
          },
        ],
      },
      {
        subtitle: { value: 'Cooking' },
        content: [
          {
            value: 'Built-in Ovens',
          },
          {
            value: 'Built-in HOBs',
          },
          {
            value: 'Hoods',
          },
          {
            value: 'Built-in Microwaves',
          },
          {
            value: 'Warning Drawers',
          },
        ],
      },
      {
        subtitle: { value: 'Dishwashers' },
        content: [
          {
            value: 'integrated Dishwashers',
          },
        ],
      },
      {
        subtitle: { value: 'Laundry' },
        content: [
          {
            value: 'Integrated Washing Machines',
          },
          {
            value: 'Integrated Washer Dryers',
          },
        ],
      },
    ],
  },
];

const NavBlock = () => (
  <div className={s.root}>
    {navColumns.map(({ column, subColumn }) => (
      <div
        className={s.column}
      >
        <Collaspsible
          trigger={column.title.value}
        >
          {
          column.content.length ? (
            column.content.map(({ value }) => (
              <p
                className={s.row}
              >
                {value}
              </p>
            ))
          )
            : (
              subColumn.map(({ subtitle, content }) => (
                <div className={s['sub-column']}>
                  <Collaspsible
                    trigger={subtitle.value}
                    triggerTagName="h1"
                  >
                    {
                    content.map(({ value }) => (
                      <p
                        key={value}
                        className={s.row}
                      >
                        {value}
                      </p>
                    ))
                    }
                  </Collaspsible>
                </div>
              ))
            )
          }
        </Collaspsible>
      </div>
    ))}
  </div>
);

export default NavBlock;
