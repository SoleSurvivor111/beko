import React from 'react';

import SubscribeBlock from './SubscribeBlock';
import NavBLock from './NavBlock';

import s from './Footer.module.scss';

const Footer = () => (
  <footer className={s.root}>
    <SubscribeBlock />
    <NavBLock />
    <div className={s.copyright}>
      <p>@ 2018 Beko Pri</p>
      <p>Terms of Service</p>
      <p>Privacy Policy</p>
      <p>Cookie Notice</p>
    </div>
  </footer>
);

export default Footer;
