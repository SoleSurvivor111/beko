export const MAX_PHOTO_UPLOADS = 9;

export const BREAKPOINT_FULL_HD = 1920;
export const BREAKPOINT_MEDIUM = 1366;
export const BREAKPOINT_DESKTOP = 1025;
export const BREAKPOINT_TABLET = 1024;
export const BREAKPOINT_TABLET_PORTRAIT = 968;
export const BREAKPOINT_MOBILE = 640;
export const BREAKPOINT_MOBILE_SMALL = 374;

export const FULL_HD_HEIGHT = 1080;

export const FULL_HD = 'FULL_HD';
export const TABLET_PORTRAIT = 'TABLET_PORTRAIT';
export const TABLET = 'TABLET';
export const DESKTOP = 'DESKTOP';
export const MEDIUM = 'MEDIUM';
export const MOBILE = 'MOBILE';
export const MOBILE_SMALL = 'MOBILE_SMALL';
