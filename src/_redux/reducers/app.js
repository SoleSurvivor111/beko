import getCurrentViewport from 'utils/getCurrentViewport';
import {
  VIEWPORT_CHANGE,
} from '../../constants';

const initialState = {
  viewport: getCurrentViewport(),
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEWPORT_CHANGE:
      return { ...state, viewport: payload };
    default:
      return state;
  }
};
