import { createSelector } from 'reselect';

const appSelector = (state) => state.app;

export const viewportSelector = createSelector(
  appSelector,
  ({ viewport }) => viewport,
);
