import {
  VIEWPORT_CHANGE,
} from '../../constants';

export const changeViewport = (viewport) => ({
  type: VIEWPORT_CHANGE,
  payload: viewport,
});
